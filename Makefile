setup:
	@echo "Setting up environment"

git:
	@echo "Setting up git hooks"
	git-credential-manager configure
	git config --global commit.template ./git/git_commit_template
	cp -f ./git_lazy/config.yml ~/.config/lazygit/config.yml
