# Git の利用法まとめ

```shell
paru -S git-credential-manager lazygit
make git
```

## Git Credential Manager

Git を利用する際は、Git Credential Manager を使用する。

- https://github.com/git-ecosystem/git-credential-manager/blob/main/docs/credstores.md

```shell
paru -S git-credential-manager
git-credential-manager configure
git config --global credential.credentialStore secretservice
```

## Git Commit Message

Git のコミットメッセージには次の形式を採用するものとする。

- https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716

その際、毎回確認できるように、commit template 機能を用いる。

```shell
git config --global commit.template .git_commit_template
```

## LazyGit

Git を Neovim から扱う際は、lazygit を用いる。
その際、commit にはカスタムコマンド Ctrl + C を用いる。

```shell
paru -S lazygit
```


